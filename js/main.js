const checkFirstname = (firstname) => {
    $(".errosFirstname").html(``)
    if(firstname === ""){
        $(".errosFirstname").html(`Yêu cầu bắt buộc nhập họ của bạn`)
        return false;
    }else{
        if(firstname.length < 2 || firstname.length > 10){
            $(".errosFirstname").html(`Yêu cầu nhập họ tối thiểu 2 và tối đa 10 ký tự`)
            return false;
        }else{
            if(firstname.includes(" ")){
                $(".errosFirstname").html(`Họ chỉ gồm 1 từ`)
                return false;
            }else{ return true}
        }
    } 
}
// ----------------------------
const checkLastname = (lastname) => {
    $(".errosLastname").html(``)
    if(lastname === ""){
        $(".errosLastname").html(`Yêu cầu bắt buộc nhập tên của bạn`)
        return false;
    }else{
        if(lastname.length < 2 || lastname.length > 20){
            $(".errosLastname").html(`Yêu cầu nhập tên tối thiểu 2 và tối đa 20 ký tự`)
            return false;
        }else{return true;}
    } 
}
// -------------------------------
const checkEmail = (email) => {
    $(".errosEmail").html('')
    if(email.includes("@fsoft.com.vn") || email.includes("@outlook.com") || email.includes("@gmail.com")){
        let index = email.indexOf("@");
        let check = email.slice(0,index);
        var emailRe = /^[a-z][a-zA-Z0-9_\.]{3,30}$/
        if(emailRe.test(check)){
            return true;
        }else{
            $(".errosEmail").html('Email gồm chữ viết thường, viết hoa, số, dấu chấm và dấu _.')
            return false;
        }
    }else{
        $(".errosEmail").html('Domain email chỉ gồm @fsoft.com.vn, @outlook.com, @gmail.com')
        return false
    }
}
// ---------------------------------
const checkPhone = (phone) => {
    $(".errosPhone").html(``)
    if(phone === ""){
        $(".errosPhone").html(`Số điện thoại là bắt buộc`)
        return false
    }else{
        if(phone.indexOf("+84") !== 0){
            $(".errosPhone").html(`Số điện thoại phải bắt đầu bằng +84`)
            return false
        }else{
            var num = phone.slice(3);
            let phoneRe = /^[0-9]{9}$/
            if( !phoneRe.test(num) || num.indexOf("0") === 0){
                $(".errosPhone").html(`Số điện thoại có độ dài 9 số và không chứa số 0 sau +84`)
                return false
            }else{
                return true;
            }
        }
    }
}
// ------------------------------
const checkGender = (gender) => {
    $(".errosGender").html(``)
    if(gender === "Nam" || gender === "Nu" || gender === "Khac"){
        return true
    }else{
        $(".errosGender").html(`Giới tính là bắt buộc`)
        return false
    }
}
// -----------------------------
const checkDate = (date) => {
    $(".errosDate").html(``)
    if(date === ""){
        $(".errosDate").html(`Ngày sinh là bắt buộc`)
        return false;
    }else{
        let datetime = date.split("-");
        datetime.forEach((item,index)=>datetime[index]=Number(item))
        let currentdate = new Date();
        if(datetime[0] < 1910 || datetime[0] > currentdate.getFullYear()){
            $(".errosDate").html(`Ngày sinh nằm trong khoảng từ 1/1/1910 đến hiện tại`)
            return false;
        }else{
            if(currentdate.getFullYear() === datetime[0]){
                if(currentdate.getMonth()+1 < datetime[1]){
                    $(".errosDate").html(`Ngày sinh nằm trong khoảng từ 1/1/1910 đến hiện tại`)
                    return false;
                }else{
                    if(currentdate.getDate() < datetime[2]){
                        $(".errosDate").html(`Ngày sinh nằm trong khoảng từ 1/1/1910 đến hiện tại`)
                        return false
                    }else{
                        return true
                    }
                }
            } else {
                return true
            }
        }
    }
}

const SubForm = ()=>{
    let firstname = $('.firstname').val().trim();
    let lastname = $('.lastname').val().trim();
    let email = $('.email').val().trim();
    let phone = $('.phone').val().trim();
    let gender = $('.gender').val();
    let date = $('.date').val().trim();
    checkFirstname(firstname)
    checkLastname(lastname)
    checkEmail(email)
    checkPhone(phone)
    checkGender(gender)
    checkDate(date)
    if(checkFirstname(firstname) && checkLastname(lastname) && checkEmail(email) && checkPhone(phone) && checkGender(gender) && checkDate(date)){
        $("#success").html(
            `
                <div class="alert alert-warning" role="alert">
					<h4>Dữ liệu đầu vào hợp lệ</h4>
				</div>
            `
        )
    }    
}